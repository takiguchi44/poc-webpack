export default class Entity {
    constructor(posX, posY) {
        this.posX = posX;
        this.posY = posY;
    }

    toString() {
        document.write(`Entity[posX:${this.posX}, posY:${this.posY}]`);
    }
}