# Démarrer l'application
Dans un terminal, lancer la commande suivante :
```bash
npm run watch
```

Puis ouvrir le fichier `index.html` dans un navigateur.

À chaque modification, enregistrez, puis rafraîchissez la page du navigateur.

# Sources
[https://www.alsacreations.com/tuto/lire/1754-debuter-avec-webpack.html](https://www.alsacreations.com/tuto/lire/1754-debuter-avec-webpack.html)